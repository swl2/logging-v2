package ru.swayfarer.loggingv2.level;

import java.util.HashMap;
import java.util.Map;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
@SuppressWarnings("unchecked")
public class LogLevel {
	
	public static final Map<String, LogLevel> registeredLevels = new HashMap<>();
	
	public static LogLevel Dev = autoRegister(builder()
			.weight(15)
			.name("Dev")
			.displayName("&c{Green}Info&c{h:1}")
			.messageColor("&c{PastelGreen}")
	.build()
	);
	
	public static LogLevel Info = autoRegister(builder()
			.weight(20)
			.name("Info")
			.displayName("&c{Green}Info&c{h:1}")
			.messageColor("&c{PastelGreen}")
	.build()
	);
	
	public static LogLevel Warning = autoRegister(builder()
			.weight(40)
			.name("Warning")
			.displayName("&c{Yellow}Warning&c{h:1}")
			.messageColor("&c{PastelYellow}")
	.build()
	);
	
	public static LogLevel Error = autoRegister(builder()
			.weight(60)
			.name("Error")
			.displayName("&c{Red}Error&c{h:1}")
			.messageColor("&c{PastelRed}")
	.build()
	);
	
	public String name;
	
	public String displayName;
	
	public String messageColor;
	
	public int weight;

	public static <T extends LogLevel> T autoRegister(T level)
	{
		registeredLevels.put(level.getName(), level);
		return level;
	}
	
	public static <T extends LogLevel> T of(String levelName)
	{
		return (T) registeredLevels.get(levelName);
	}
}

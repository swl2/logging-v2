package ru.swayfarer.loggingv2.handlers.stacktracefilter;

import ru.swayfarer.loggingv2.event.LogEvent;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1NoR;
import ru.swayfarer.swl2.string.StringUtils;

public class StackTraceFilter implements IFunction1NoR<LogEvent> {

	public IExtendedList<String> stackTraceBlocks = CollectionsSWL.createExtendedList();
	
	@Override
	public void applyNoR(LogEvent event) {
		
		if (event.hasAttachedThrowable())
		{
			var attachedThrowableInfo = event.getAttachedThrowableInfo();
			var stackTrace = attachedThrowableInfo.getStackTraceElements();
			
			IExtendedList<StackTraceElement> newStacktrace = CollectionsSWL.createExtendedList(stackTrace);
			
			for (var stacktraceElement : stackTrace)
			{
				var className = stacktraceElement.getClassName();
				
				for (var block : stackTraceBlocks)
				{
					if (StringUtils.isMatchesByExpression(block, className))
					{
						newStacktrace.remove(stacktraceElement);
						break;
					}
				}
			}
			
			attachedThrowableInfo.setStackTraceElements(newStacktrace.toArray(StackTraceElement.class));
		}
	}

}

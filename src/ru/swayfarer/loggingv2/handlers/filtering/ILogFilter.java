package ru.swayfarer.loggingv2.handlers.filtering;

import ru.swayfarer.loggingv2.event.LogEvent;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1;

public interface ILogFilter extends IFunction1<LogEvent, Boolean> {

}

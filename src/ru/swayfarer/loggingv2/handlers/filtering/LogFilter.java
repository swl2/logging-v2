package ru.swayfarer.loggingv2.handlers.filtering;

import ru.swayfarer.loggingv2.event.LogEvent;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1NoR;

public class LogFilter implements IFunction1NoR<LogEvent> {

	public IExtendedList<ILogFilter> filters = CollectionsSWL.createExtendedList();
	
	@Override
	public void applyNoR(LogEvent event) {
		
		for (var fun : filters)
		{
			if (!fun.apply(event))
			{
				event.setCanceled(true);
				return;
			}
		}
	}
}

package ru.swayfarer.loggingv2.handlers.filtering;

import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.collections.streams.DataStream;
import ru.swayfarer.swl2.string.StringUtils;

public interface StandartLogFilters {

	public static ILogFilter levelFilter(int min, int max)
	{
		return (evt) -> {
			var weight = evt.getLevel().getWeight();
			return min <= weight && max >= weight;
		};
	}
	
	public static ILogFilter expressions(boolean isWhiteList, IExtendedList<String> expressions)
	{
		return (evt) -> {
			var className = evt.getLogSource().getCallerStacktrace();
			
			for (var expression : expressions)
			{
				if (StringUtils.isMatchesByExpression(expression, className))
				{
					return !isWhiteList;
				}
			}
			
			return isWhiteList;
		};
	}
	
	public static ILogFilter onlyLevels(String... names)
	{
		return (evt) -> DataStream.of(names).contains(evt.getLevel().getName());
	}
	
	public static ILogFilter notLevels(String... names)
	{
		return (evt) -> !DataStream.of(names).contains(evt.getLevel().getName());
	}
}

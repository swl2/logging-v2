package ru.swayfarer.loggingv2.handlers.logformatter;

import java.util.Date;

import lombok.Builder;
import lombok.NonNull;
import ru.swayfarer.loggingv2.event.LogEvent;
import ru.swayfarer.loggingv2.handlers.logformatter.LogFormatter.ILogFormatRule;
import ru.swayfarer.swl2.date.DateUtils;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1;

public interface StandartLogFormatRules {

	public static final ILogFormatRule LOG_LEVEL_RULE = replace("%level%", (evt) -> {
		var level = evt.getLevel();
		return level == null ? "" : level.getDisplayName();
	});

	public static final ILogFormatRule LOG_LEVEL_COLOR_RULE = replace("%levelColor%", (evt) -> {
		var level = evt.getLevel();
		return level == null ? "" : level.getMessageColor();
	});
	
	public static final ILogFormatRule LOG_NAME_RULE = replace("%name%", (evt) -> {
		var logSource = evt.getLogSource();
		
		if (logSource == null)
			return "";
		
		var logger = logSource.getLogger();
		
		return logger == null ? "" : logger.name;
	});
	
	public static final ILogFormatRule LOG_THREAD_RULE = replace("%thread%", (evt) -> {
		var logSource = evt.getLogSource();
		
		if (logSource == null)
			return "";
		
		var thread = logSource.getLogThread();
		
		return thread == null ? "" : thread.getName();
	});
	
	public static final ILogFormatRule LOG_CONTENT_RULE = replace("%content%", (evt) -> evt.getContent());
	
	public static final ILogFormatRule LOG_DATE_RULE = (content, event) -> {
		
		if (content.contains(DateUtils.DATE_REGEX_START) || content.contains(DateUtils.DATE_REGEX_END))
		{
			return DateUtils.replaceAllDates(content, new Date(event.getLogSource().getCreationTime()));
		}
		
		return content;
	};
	
	public static final ILogFormatRule LOG_FROM_RULE = replace("%from%", (evt) -> {
		
		var logSource = evt.getLogSource();
		
		if (logSource == null)
			return "";
		
		var callerStacktrace = logSource.getCallerStacktrace();
		
		if (callerStacktrace == null)
			return "";
		
		var fileName = callerStacktrace.getFileName();
		var line = evt.getLogSource().getCallerStacktrace().getLineNumber();
		return fileName + ":" + line;
	});
	
	public static ILogFormatRule replace(String prefix, IFunction1<LogEvent, String> fun)
	{
		return ReplaceFormatRule.builder()
				.prefix(prefix)
				.textFun(fun)
		.build();
	}
	

	
	@Builder
	public static class ReplaceFormatRule implements ILogFormatRule {

		@NonNull
		public String prefix;
		
		@NonNull
		public IFunction1<LogEvent, String> textFun;
		
		@Override
		public String apply(String format, LogEvent logEvent)
		{
			if (format.contains(prefix))
			{
				String replace = format.replace(prefix, textFun.apply(logEvent));
				return replace;
			}
			
			return format;
		}
	}
}

package ru.swayfarer.loggingv2.handlers.logformatter;

import lombok.val;
import ru.swayfarer.loggingv2.event.LogEvent;
import ru.swayfarer.loggingv2.event.LogEvent.StackTraceEntry;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1NoR;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction2;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction3;

@SuppressWarnings("unchecked")
public class LogFormatter implements IFunction1NoR<LogEvent> {
	
	public static String defaultFormat = "[%thread%/%level%] [%name%] -> %content%";
	public static String defaultStackTraceFormat = "at %class%.%method%(%file%:%line%) [~%loc%]";
	public static String defaultStackTraceHeaderFormat = "%logColor%&c{PastelCyan}%type%&c{h:1} %message%";
	
	public IExtendedList<ILogFormatRule> formatRules = CollectionsSWL.createExtendedList();
	public IExtendedList<ILogStackTraceFormatRule> stacktraceFormatRules = CollectionsSWL.createExtendedList();
	public IExtendedList<ILogStackTraceHeaderFormatRule> stacktraceHeaderFormatRules = CollectionsSWL.createExtendedList();
	
	public ILogFormatGetFun formatGetFun = (e) -> defaultFormat;
	public ILogFormatGetFun stackTraceFormatGetFun = (e) -> defaultStackTraceFormat;
	public ILogFormatGetFun stackTraceHeaderFormatGetFun = (e) -> defaultStackTraceHeaderFormat;
	
	@Override
	public void applyNoR(LogEvent event)
	{
		formatLogContent(event);
		formatStackTraceContent(event);
	}
	
	public void formatStackTraceContent(LogEvent event)
	{
		if (event.hasAttachedThrowable())
		{
			var throwableInfo = event.getAttachedThrowableInfo();
			IExtendedList<StackTraceEntry> content = CollectionsSWL.createExtendedList();
			
			while (throwableInfo != null)
			{
				var stacktraceElements = event.getAttachedThrowableInfo().getStackTraceElements();
				fillStacktraceContents(throwableInfo.getAttachedThrowable(), event, content, stacktraceElements);
				
				throwableInfo = throwableInfo.getCause();
			}
			
			event.setThrowableContents(content);
		}
	}
	
	public void fillStacktraceContents(Throwable e, LogEvent event, IExtendedList<StackTraceEntry> content, StackTraceElement[] stacktraceElements)
	{
		var headerContent = stackTraceHeaderFormatGetFun.apply(event);
		
		for (var fun : stacktraceHeaderFormatRules)
		{
			headerContent = fun.apply(headerContent, event, e);
		}
		
		content.add(StackTraceEntry.builder()
				.content(headerContent)
				.isHeader(true)
				.stackTraceElement(null)
		.build());
		
		for (int i1 = 0; i1 < stacktraceElements.length; i1 ++)
		{
			var line = stackTraceFormatGetFun.apply(event);
			
			for (var fun : stacktraceFormatRules)
			{
				line = fun.apply(line, event, stacktraceElements[i1]);
			}
			
			content.add(StackTraceEntry.builder()
					.content(line)
					.stackTraceElement(stacktraceElements[i1])
					.isHeader(false)
			.build())
			;
		}
	}
	
	public void formatLogContent(LogEvent event)
	{
		String format = formatGetFun.apply(event);
		
		for (val formatFun : formatRules)
		{
			format = formatFun.apply(format, event);
		}
		
		event.setContent(format);
	}
	
	public <T extends LogFormatter> T registerStacktraceFormatRule(ILogStackTraceFormatRule fun)
	{
		this.stacktraceFormatRules.addExclusive(fun);
		return (T) this;
	}
	
	public <T extends LogFormatter> T registerStacktraceHeaderFormatRule(ILogStackTraceHeaderFormatRule fun)
	{
		this.stacktraceHeaderFormatRules.addExclusive(fun);
		return (T) this;
	}
	
	public <T extends LogFormatter> T registerFormatRule(ILogFormatRule fun)
	{
		this.formatRules.addExclusive(fun);
		return (T) this;
	}
	
	public <T extends LogFormatter> T registerDefaultFormatRules()
	{
		registerFormatRule(StandartLogFormatRules.LOG_LEVEL_COLOR_RULE);
		registerFormatRule(StandartLogFormatRules.LOG_THREAD_RULE);
		registerFormatRule(StandartLogFormatRules.LOG_NAME_RULE);
		registerFormatRule(StandartLogFormatRules.LOG_LEVEL_RULE);
		registerFormatRule(StandartLogFormatRules.LOG_FROM_RULE);
		registerFormatRule(StandartLogFormatRules.LOG_CONTENT_RULE);
		registerFormatRule(StandartLogFormatRules.LOG_DATE_RULE);

		registerStacktraceHeaderFormatRule(StandartStacktraceHeaderFormatRules.TYPE_RULE);
		registerStacktraceHeaderFormatRule(StandartStacktraceHeaderFormatRules.MESSAGE_RULE);
		registerStacktraceHeaderFormatRule(StandartStacktraceHeaderFormatRules.LEVEL_RULE);
		
		registerStacktraceFormatRule(StandartStacktraceFormatRules.TRACE_CLASS_FUN);
		registerStacktraceFormatRule(StandartStacktraceFormatRules.TRACE_FILE_FUN);
		registerStacktraceFormatRule(StandartStacktraceFormatRules.TRACE_LINE_FUN);
		registerStacktraceFormatRule(StandartStacktraceFormatRules.TRACE_METHOD_FUN);
		registerStacktraceFormatRule(StandartStacktraceFormatRules.TRACE_LOCATION_FUN);
		
		return (T) this;
	}

	public static interface ILogStackTraceFormatRule extends IFunction3<String, LogEvent, StackTraceElement, String> {};
	
	public static interface ILogStackTraceHeaderFormatRule extends IFunction3<String, LogEvent, Throwable, String> {};
	
	public static interface ILogFormatRule extends IFunction2<String, LogEvent, String> {};
	public static interface ILogFormatGetFun extends IFunction1<LogEvent, String> {};
}

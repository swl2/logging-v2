package ru.swayfarer.loggingv2.handlers.logformatter;

import lombok.Builder;
import ru.swayfarer.loggingv2.event.LogEvent;
import ru.swayfarer.loggingv2.handlers.logformatter.LogFormatter.ILogStackTraceFormatRule;
import ru.swayfarer.swl2.classes.ReflectionUtils;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction4;

public interface StandartStacktraceFormatRules extends StandartLogFormatRules {

	public static ILogStackTraceFormatRule TRACE_FILE_FUN = replace("%file%", (e) -> e.getFileName());
	public static ILogStackTraceFormatRule TRACE_CLASS_FUN = replace("%class%", (e) -> e.getClassName());
	public static ILogStackTraceFormatRule TRACE_LINE_FUN = replace("%line%", (e) -> String.valueOf(e.getLineNumber()));
	public static ILogStackTraceFormatRule TRACE_METHOD_FUN = replace("%method%", (e) -> e.getMethodName());
	public static ILogStackTraceFormatRule TRACE_LOCATION_FUN = replace("%loc%", (e) -> ReflectionUtils.getClassSource(e.getClassName()));
	
	public static ILogStackTraceFormatRule replace(String prefix, IFunction1<StackTraceElement, String> fun)
	{
		return StacktraceReplaceRule.builder()
				.prefix(prefix)
				.newTextFun((format, e, event, stackTraceElement) -> fun.apply(stackTraceElement))
		.build();
	}
	
	public static ILogStackTraceFormatRule replace(String prefix, IReplaceFormatFun fun)
	{
		return StacktraceReplaceRule.builder()
				.prefix(prefix)
				.newTextFun(fun)
		.build();
	}
	
	@Builder
	public static class StacktraceReplaceRule implements ILogStackTraceFormatRule {

		public String prefix;
		public IReplaceFormatFun newTextFun;
		
		@Override
		public String apply(String format, LogEvent event, StackTraceElement stackTraceElement) {
			
			if (format.contains(prefix))
			{
				format = format.replace(prefix, newTextFun.apply(format, event.getAttachedThrowableInfo().getAttachedThrowable(), event, stackTraceElement));
			}
			
			return format;
		}
		
	}
	
	public static interface IReplaceFormatFun extends IFunction4<String, Throwable, LogEvent, StackTraceElement, String> {}
	
}

package ru.swayfarer.loggingv2.handlers.logformatter;

import java.util.Map;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.loggingv2.event.LogEvent;
import ru.swayfarer.loggingv2.handlers.colorizer.LogColorizer;
import ru.swayfarer.loggingv2.handlers.logformatter.LogFormatter.ILogFormatGetFun;
import ru.swayfarer.loggingv2.handlers.logformatter.LogFormatter.ILogFormatRule;
import ru.swayfarer.loggingv2.level.LogLevel;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;

/**
 * Идея в том, чтобы подставить все зависящее от уровня лога, раскрасить, а потом кэшировать. <br> 
 * Так будет намного быстрее 
 * @author swayfarer
 *
 */
@Getter @Setter @Accessors(chain = true)
@SuppressWarnings("unchecked")
public class ColorizedLogFormatFun implements ILogFormatGetFun {

	public String format = "";
	
	public LogColorizer logColorizer = new LogColorizer();
	
	public ThreadLocal<Map<LogLevel, String>> cachedFormats = new ThreadLocal<>() {
		@Override
		protected Map<LogLevel, String> initialValue() {
			return CollectionsSWL.createIdentityMap();
		}
	};
	
	public IExtendedList<ILogFormatRule> preProcessRules = CollectionsSWL.createExtendedList();
	
	@Override
	public String apply(LogEvent event)
	{
		return getOrCreateFormat(event.getLevel());
	}
	
	public String getOrCreateFormat(LogLevel level)
	{
		var ret = cachedFormats().get(level);
		
		if (ret != null)
			return ret;
		
		ret = getFormat(level);
		
		cachedFormats().put(level, ret);
		
		return ret;
	}
	
	private Map<LogLevel, String> cachedFormats() {
		return cachedFormats.get();
	}

	public String getFormat(LogLevel level)
	{
		var ret = format;
		
		var fakeLogEvent = LogEvent.builder()
				.content(format)
				.level(level)
				.logSource(null)
		.build();
		
		for (var fun : preProcessRules)
		{
			ret = fun.apply(ret, fakeLogEvent);
		}
		
		ret = logColorizer.colorize(ret);
		
		return ret;
	}
	
	public <T extends ColorizedLogFormatFun> T registerPreProcessRule(ILogFormatRule fun)
	{
		this.preProcessRules.addExclusive(fun);
		return (T) this;
	}
	
	public <T extends ColorizedLogFormatFun> T registerDefaultPreProcessRules()
	{
		registerPreProcessRule(StandartLogFormatRules.LOG_LEVEL_COLOR_RULE);
		registerPreProcessRule(StandartLogFormatRules.LOG_LEVEL_RULE);
		return (T) this;
	}
}

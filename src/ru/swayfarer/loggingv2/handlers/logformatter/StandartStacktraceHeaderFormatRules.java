package ru.swayfarer.loggingv2.handlers.logformatter;

import lombok.Builder;
import lombok.Data;
import ru.swayfarer.loggingv2.event.LogEvent;
import ru.swayfarer.loggingv2.handlers.logformatter.LogFormatter.ILogStackTraceHeaderFormatRule;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction2;

public interface StandartStacktraceHeaderFormatRules {

	public static ReplaceRule TYPE_RULE = replaceRule("%type%", (e) -> e.getClass().getName());
	public static ReplaceRule MESSAGE_RULE = replaceRule("%message%", (e) -> e.getMessage());
	public static ReplaceRule LEVEL_RULE = replaceRule("%levelColor%", (event, e) -> event.getLevel().getMessageColor());
	
	public static ReplaceRule replaceRule(String prefix, IFunction1<Throwable, String> fun)
	{
		return replaceRule(prefix, (event, e) -> fun.apply(e));
	}
	
	public static ReplaceRule replaceRule(String prefix, IFunction2<LogEvent, Throwable, String> fun)
	{
		return ReplaceRule.builder()
				.fun(fun)
				.prefix(prefix)
		.build();
	}
	
	@Data
	@Builder
	public static class ReplaceRule implements ILogStackTraceHeaderFormatRule {

		public String prefix;
		public IFunction2<LogEvent, Throwable, String> fun;
		
		@Override
		public String apply(String content, LogEvent event, Throwable e) {
			
			if (content.contains(prefix))
			{
				var message = fun.apply(event, e);
				
				if (message == null)
					message = "";
				
				content = content.replace(prefix, message);
			}
			
			return content;
		}
		
	}
	
}

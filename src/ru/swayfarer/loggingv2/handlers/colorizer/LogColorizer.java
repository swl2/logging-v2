package ru.swayfarer.loggingv2.handlers.colorizer;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.loggingv2.event.LogEvent;
import ru.swayfarer.swl2.ansi.Ansi;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1NoR;
import ru.swayfarer.swl2.string.DynamicString;
import ru.swayfarer.swl2.string.StringUtils;
import ru.swayfarer.swl2.string.reader.StringReaderSWL;

@Getter @Setter @Accessors(chain = true)
public class LogColorizer implements IFunction1NoR<LogEvent> {

	@NonNull
	public IFunction1<String, String> codeToColorFun;
	
	public String codeStart = "&c{";
	public String codeEnd = "}";
	
	public String colorRegex = StringUtils.regex()
			.text(codeStart)
				.some()
				.not("}")
			.text(codeEnd)
	.build();
	
	@Override
	public void applyNoR(LogEvent logEvent)
	{
		var content = logEvent.getContent();
		
		if (content.contains(codeStart) && content.contains(codeEnd))
			logEvent.setContent(colorize(logEvent.getContent()));
	}
	
	public String colorize(String content)
	{
		var buffer = new DynamicString();
		var idBuffer = new DynamicString();
		var reader = new StringReaderSWL(content);
		var colorsHistory = CollectionsSWL.createExtendedList();
		
		boolean isInPrefix = false;
		
		while (reader.hasNextElement())
		{
			if (isInPrefix)
			{
				if (reader.skipSome(codeEnd))
				{
					isInPrefix = false;
					
					var colorCode = idBuffer.toString();
					idBuffer.clear();
					
					if (!StringUtils.isBlank(colorCode))
					{
						if (colorCode.equals("clear"))
						{
							buffer.append(Ansi.SANE);
						}
						else if (colorCode.startsWith("h:"))
						{
							var idStr = colorCode.substring(2);
							
							if (StringUtils.isInteger(idStr))
							{
								var id = Integer.valueOf(idStr);
								
								if (id < 0)
									id = -id;
								
								var size = colorsHistory.size();
								
								id = size - id - 1;
								
								var historyColorPrefix = size > id && id >= 0 ? colorsHistory.get(id) : Ansi.SANE;
								
								buffer.append(historyColorPrefix);
								colorsHistory.removeLastElement();
							}
						}
						else
						{
							var colorPrefix = codeToColorFun.apply(colorCode);
							
							if (colorPrefix != null)
							{
								colorsHistory.add(colorPrefix);
								buffer.append(colorPrefix);
							}
						}
					}
					
				}
				else
				{
					idBuffer.append(reader.next());
				}
			}
			else 
			{
				if (reader.skipSome(codeStart))
				{
					isInPrefix = true;
				}
				else
				{
					buffer.append(reader.next());
				}
			}
		}
		
		reader.close();
		
		return buffer.toString();
	}
	
	public String colorize2(String content)
	{
		if (!(content.contains(codeStart) && content.contains(codeEnd)))
			return content;
		
		var colors = StringUtils.getAllMatches(colorRegex, content);
		
		if (!CollectionsSWL.isNullOrEmpty(colors))
		{
			colors.distinct();
			
			IExtendedList<String> colorsHistory = CollectionsSWL.createExtendedList();
			
			for (var color : colors)
			{
				var colorCode = StringUtils.subString(codeStart.length(), -codeEnd.length(), color);
				
				if (!StringUtils.isBlank(colorCode))
				{
					if (colorCode.equals("clear"))
					{
						content = content.replace(color, Ansi.SANE);
					}
					else if (colorCode.startsWith("h:"))
					{
						var idStr = colorCode.substring(2);
						
						if (StringUtils.isInteger(idStr))
						{
							var id = Integer.valueOf(idStr);
							
							if (id < 0)
								id = -id;
							
							var size = colorsHistory.size();
							
							id = size - id - 1;
							
							var historyColorPrefix = size > id && id >= 0 ? colorsHistory.get(id) : Ansi.SANE;
							
							content = content.replace(color, historyColorPrefix);
							colorsHistory.removeLastElement();
						}
					}
					else
					{
						var colorPrefix = codeToColorFun.apply(colorCode);
						
						if (colorPrefix != null)
						{
							colorsHistory.add(colorPrefix);
							content = content.replace(color, colorPrefix);
						}
					}
				}
			}
		}
		
		return content;
	}
}

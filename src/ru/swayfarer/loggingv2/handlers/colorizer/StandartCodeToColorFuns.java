package ru.swayfarer.loggingv2.handlers.colorizer;

import java.util.HashMap;
import java.util.Map;

import lombok.Builder;
import lombok.Singular;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1;

public interface StandartCodeToColorFuns {

	public static final Map<String, IFunction1<String, String>> registeredColorFuns = new HashMap<>();
	
	public static CodeToColorFun ENABLE_8BIT = CodeToColorFun.builder()
			.defaultFun((code) -> "\033[38;5;" + code + "m")
			
			.color("Black", "0")
			.color("Red", "1")
			.color("Green", "2")
			.color("Yellow", "3")
			.color("Blue", "4")
			.color("Magnetta", "5")
			.color("Cyan", "6")
			.color("White", "7")
			.color("Grey", "7")
			
			.color("BrightBlack", "8")
			.color("DarkGrey", "8")
			.color("BrightRed", "9")
			.color("BrightGreen", "85")
			.color("BrightYellow", "11")
			.color("BrightYellow", "12")
			.color("BrightMagnetta", "13")
			.color("BrightCyan", "14")
			.color("BrightWhite", "15")
			
			.color("PastelRed", "203")
			.color("PastelGreen", "85")
			.color("PastelYellow", "229")
			.color("PastelBlue", "75")
			.color("PastelMagnetta", "91")
			.color("PastelCyan", "153")
			.color("PastelCyanBright", "195")
			.color("PastelWhite", "7")
			.color("PastelGrey", "7")
			
	.build()
	.register("8bit");
	
	public static CodeToColorFun ENABLE_ANCI = CodeToColorFun.builder()
			.defaultFun((code) -> "\033[" + code + "m")
			
			.color("Black", "30")
			.color("Red", "31")
			.color("Green", "32")
			.color("Yellow", "33")
			.color("Blue", "34")
			.color("Magnetta", "35")
			.color("Cyan", "36")
			.color("White", "37")
			.color("Grey", "37")
			
			.color("BrightBlack", "90")
			.color("DarkGrey", "90")
			.color("BrightRed", "91")
			.color("BrightGreen", "92")
			.color("BrightYellow", "93")
			.color("BrightYellow", "94")
			.color("BrightMagnetta", "95")
			.color("BrightCyan", "96")
			.color("BrightWhite", "97")
			
			// В ANCI Пастэльных цветов нет, так что просто используем обычные 
			.color("PastelBlack", "30")
			.color("PastelRed", "31")
			.color("PastelGreen", "32")
			.color("PastelYellow", "33")
			.color("PastelBlue", "34")
			.color("PastelMagnetta", "35")
			.color("PastelCyan", "36")
			.color("PastelWhite", "37")
			.color("PastelGrey", "37")
	.build()
	.register("anci");
	
	public static IFunction1<String, String> DISABLE = getDisabled("disabled");

	public static IFunction1<String, String> getDisabled(String str)
	{
		IFunction1<String, String> fun = (code) -> "";
		registeredColorFuns.put(str, fun);
		return fun;
	}
	
	@Builder
	public static class CodeToColorFun implements IFunction1<String, String>{

		public IFunction1<String, String> defaultFun;
		
		@Singular
		public Map<String, String> colors;
		
		@Override
		public String apply(String code)
		{
			var registeredColor = colors.get(code);
			
			if (registeredColor != null)
				code = registeredColor;
			
			return defaultFun.apply(code);
		}
		
		@SuppressWarnings("unchecked")
		public <T extends CodeToColorFun> T register(String name)
		{
			registeredColorFuns.put(name, this);
			return (T) this;
		}
		
	}

	public static IFunction1<String, String> of(String coloringMode) {
		return registeredColorFuns.get(coloringMode);
	}
}

package ru.swayfarer.loggingv2.handlers.io;

import java.io.IOException;
import java.util.Map;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1NoR;
import ru.swayfarer.swl2.observable.IObservable;
import ru.swayfarer.swl2.observable.Observables;
import ru.swayfarer.swl2.observable.events.AbstractCancelableEvent;
import ru.swayfarer.swl2.resource.file.FileSWL;

public class FileWriterFun implements IFunction1NoR<String> {

	public String encoding;
	
	public FileSWL file;
	
	public IObservable<StringWritingEvent> eventWrite = Observables.createObservable();
	
	public static Map<String, FileWriterFun> cachedFuns = CollectionsSWL.createWeakMap();
	
	@Override
	public void applyNoR(String str) {
		
		file.locked(() -> {
			
			try
			{
				var event = StringWritingEvent.builder()
						.file(file)
						.str(str)
				.build();
				
				if (event.isCanceled())
					return;
				
				eventWrite.next(event);
				
				var append = file.append();
				append.write(str.getBytes(encoding));
				append.close();
			}
			catch (Throwable e)
			{
				System.err.println("Can't write file " + file);
				e.printStackTrace();
			}
			
		});
	}
	
	public static FileWriterFun of(FileSWL file, String encoding) throws IOException 
	{
		var path = file.getCanonicalPath();
		var cached = cachedFuns.get(path);
		
		if (cached != null)
			return cached;
		
		file.createIfNotFound();
		FileWriterFun ret = new FileWriterFun();
		ret.file = file;
		ret.encoding = encoding;
		
		cachedFuns.put(path, ret);
		
		return ret;
	}
	
	@Builder
	@Data @EqualsAndHashCode(callSuper = false)
	public static class StringWritingEvent extends AbstractCancelableEvent {
		public FileSWL file;
		public String str;
	}
}
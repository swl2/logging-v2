package ru.swayfarer.loggingv2.handlers.io;

import ru.swayfarer.loggingv2.event.LogEvent;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1NoR;

public class LogWriter implements IFunction1NoR<LogEvent> {

	public IExtendedList<IWriteAccess> accesses = CollectionsSWL.createExtendedList();
	
	@Override
	public void applyNoR(LogEvent event) {
		
		for (var access : accesses)
		{
			access.apply(event.getContentFully());
		}
	}
}

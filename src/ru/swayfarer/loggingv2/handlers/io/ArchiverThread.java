package ru.swayfarer.loggingv2.handlers.io;

import java.io.OutputStream;
import java.util.concurrent.atomic.AtomicLong;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1;
import ru.swayfarer.swl2.resource.file.FileSWL;
import ru.swayfarer.swl2.resource.streams.DataInputStreamSWL;
import ru.swayfarer.swl2.resource.streams.StreamsUtils;

@Getter @Setter @Accessors(chain = true)
public class ArchiverThread extends Thread {

	public FileSWL file;
	
	public AtomicLong sleepTimePerPoint = new AtomicLong(30 * 1000);

	public AtomicLong sleepTime = new AtomicLong();
	
	public AtomicLong currentSleepTime = new AtomicLong();
	
	public IFunction1<FileSWL, OutputStream> streamCreationFun;
	
	public IFunction1<FileSWL, FileSWL> archiveCreationFun;
	
	public IExtendedList<IFunction1<FileSWL, Boolean>> conditions = CollectionsSWL.createExtendedList();
	
	public ArchiverThread()
	{
		setDaemon(true);
	}
	
	@Override
	public void run() {
		
		for (;;)
		{
			try
			{
				for (var fun : conditions)
				{
					if (fun.apply(file))
					{
						archive();
						break;
					}
				}
				
				startSleep();
			}
			catch (Throwable e)
			{
				System.err.println("Error while archiving file " + file);
				e.printStackTrace();
			}
		}
	}
	
	public void startSleep() throws InterruptedException
	{
		long sleepTimePerPoint = this.sleepTimePerPoint.get();
		long time = currentSleepTime.get();
		
		if (sleepTimePerPoint <= 0)
			return;
		
		if (time < 0)
			currentSleepTime.set(0);
		else if (time > 0)
		{
			currentSleepTime.decrementAndGet();
			Thread.sleep(sleepTimePerPoint * currentSleepTime.getAndIncrement());
		}
	}
	
	public void archive()
	{
		file.locked(() -> {
			
			try
			{
				var archiveFile = archiveCreationFun.apply(this.file);
				var os = streamCreationFun.apply(archiveFile);
				DataInputStreamSWL in = file.in();
				StreamsUtils.copyStream(in, os, true, true);
				file.remove();
			}
			catch (Throwable e)
			{
				System.err.println("Error while archiving file " + file);
				e.printStackTrace();
			}
			
		});
	}
}

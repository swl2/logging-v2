package ru.swayfarer.loggingv2.handlers.io;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.atomic.AtomicLong;

import lombok.Builder;
import lombok.NonNull;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1NoR;

@Builder
public class WriterThread extends Thread {

	@Builder.Default
	public AtomicLong sleepTime = new AtomicLong();
	
	@NonNull
	public IFunction1NoR<String> writerFun;
	
	@Builder.Default
	public Queue<String> linesQueue = new ConcurrentLinkedDeque<>();
	
	{
		setDaemon(true);
		Runtime.getRuntime().addShutdownHook(new Thread(this::write));
	}
	
	@Override
	public void run() {
		
		for (;;)
		{
			write();
			
			try
			{
				Thread.sleep(sleepTime.get());
			}
			catch (Throwable e)
			{
				System.err.println("Error while skip " + sleepTime + " milisis");
			}
		}
	}
	
	public void write()
	{
		String line;
		
		while ((line = linesQueue.poll()) != null)
		{
			try
			{
				writerFun.apply(line);
			}
			catch (Throwable e)
			{
				System.err.println("Error while writing line " + line + " by " + this.getName());
				e.printStackTrace();
			}
		}
	}
	
	public IWriteAccess getAccess()
	{
		return (s) -> linesQueue.add(s);
	}
}

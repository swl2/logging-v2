package ru.swayfarer.loggingv2.handlers.io;

import java.util.concurrent.atomic.AtomicLong;

import ru.swayfarer.loggingv2.handlers.io.ArchiverWriteListener.IArchiveCondition;
import ru.swayfarer.swl2.resource.file.FileSWL;

@SuppressWarnings("unchecked")
public interface StandartArchiveCondition {

	public static IArchiveCondition everyWeek(long timeInWeeks)
	{
		return everyHour(timeInWeeks * 7);
	}
	
	public static IArchiveCondition everyDay(long timeInDays)
	{
		return everyHour(timeInDays * 24);
	}
	
	public static IArchiveCondition everyHour(long timeInHours)
	{
		return everyMin(timeInHours * 60);
	}
	
	public static IArchiveCondition everyMin(long timeInMin)
	{
		return everySec(timeInMin * 60);
	}
	
	public static IArchiveCondition everySec(long timeInSec)
	{
		return every(timeInSec * 1000);
	}
	
	public static IArchiveCondition every(long timeInMilisis)
	{
		return new IArchiveCondition() {

			public AtomicLong startTime = new AtomicLong(System.currentTimeMillis());
			
			@Override
			public Boolean apply(FileSWL file) {
				
				var currentTime = System.currentTimeMillis();
				var time = currentTime - startTime.get();
				
				if (time > timeInMilisis)
				{
					startTime.set(time);
					return true;
				}
				
				return false;
			}

			@Override
			public <T extends IArchiveCondition> T reset() {
				startTime.set(System.currentTimeMillis());
				return (T) this;
			}
		};
	}
	
	public static IArchiveCondition maxSize(long sizeInBytes)
	{
		return new IArchiveCondition() {
			
			@Override
			public Boolean apply(FileSWL f) {
				return f.length() > sizeInBytes;
			}
			
			@Override
			public <T extends IArchiveCondition> T reset() {
				return (T) this;
			}
		};
	}
}

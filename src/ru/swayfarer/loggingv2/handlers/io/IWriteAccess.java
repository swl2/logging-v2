package ru.swayfarer.loggingv2.handlers.io;

import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1NoR;

public interface IWriteAccess extends IFunction1NoR<String> {
}

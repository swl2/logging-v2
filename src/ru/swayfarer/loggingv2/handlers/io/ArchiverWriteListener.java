package ru.swayfarer.loggingv2.handlers.io;

import java.io.OutputStream;

import lombok.Builder;
import lombok.NonNull;
import lombok.SneakyThrows;
import ru.swayfarer.loggingv2.handlers.io.FileWriterFun.StringWritingEvent;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1NoR;
import ru.swayfarer.swl2.resource.file.FileSWL;
import ru.swayfarer.swl2.resource.streams.DataInputStreamSWL;
import ru.swayfarer.swl2.resource.streams.StreamsUtils;
import ru.swayfarer.swl2.tasks.ITask;
import ru.swayfarer.swl2.tasks.factories.ThreadPoolTaskFactory;

@Builder
@SuppressWarnings("unchecked")
public class ArchiverWriteListener implements IFunction1NoR<StringWritingEvent> {

	public String encoding;
	
	@Builder.Default
	public IExtendedList<IArchiveCondition> conditions = CollectionsSWL.createExtendedList();
	
	@Builder.Default
	public IFunction1<Runnable, ITask> executor = new ThreadPoolTaskFactory(500, 1);
	
	public FileSWL file;
	
	@NonNull
	public IFunction1<FileSWL, OutputStream> streamCreationFun;
	
	@NonNull
	public IFunction1<FileSWL, FileSWL> archiveCreationFun;
	
	@Override
	public void applyNoR(StringWritingEvent event) {
		
		var file = event.getFile();
		
		if (file == this.file)
		{
			executor.apply(() -> {
				file.locked(() -> {
					archive();
				});
			});
		}
	}

	@SneakyThrows
	public void archive()
	{
		for (var conditionFun : conditions)
		{
			if (conditionFun.apply(file))
			{
				var archiveFile = archiveCreationFun.apply(this.file);
				var os = streamCreationFun.apply(archiveFile);
				DataInputStreamSWL in = file.in();
				StreamsUtils.copyStream(in, os, true, true);
				file.remove();
				resetConditions();
				
				break;
			}
		}
	}
	
	public void resetConditions()
	{
		for (var conditionFun : conditions)
		{
			if (conditionFun.canReset())
				conditionFun.reset();
		}
	}
	
	public static interface IArchiveCondition extends IFunction1<FileSWL, Boolean> {
		
		public default <T extends IArchiveCondition> T reset()
		{
			return (T) this;
		}

		public default boolean canReset() 
		{
			return true;
		}
	}
}

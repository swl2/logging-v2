package ru.swayfarer.loggingv2;

import ru.swayfarer.loggingv2.event.AttachedThrowableInfo;
import ru.swayfarer.loggingv2.event.LogEvent;
import ru.swayfarer.loggingv2.event.LogSource;
import ru.swayfarer.loggingv2.level.LogLevel;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1NoR;
import ru.swayfarer.swl2.observable.IObservable;
import ru.swayfarer.swl2.observable.Observables;
import ru.swayfarer.swl2.string.StringUtils;

public class LoggerV2 {

	public String name = "123";
	
	public IFunction1NoR<String> printerFun = (s) -> System.out.print(s);
	
	public IObservable<LogEvent> eventPreLog = Observables.createObservable();
	public IObservable<LogEvent> eventPostLog = Observables.createObservable();
	
	
	public void dev(Object... text)
	{
		log(null, StringUtils.concatWithSpaces(text), LogLevel.Dev, 1);
	}
	
	public void info(Object... text)
	{
		log(null, StringUtils.concatWithSpaces(text), LogLevel.Info, 1);
	}
	
	public void warning(Object... text)
	{
		log(null, StringUtils.concatWithSpaces(text), LogLevel.Warning, 1);
	}
	
	public void error(Object... text)
	{
		log(null, StringUtils.concatWithSpaces(text), LogLevel.Error, 1);
	}
	
	
	
	public void dev(Throwable e, Object... text)
	{
		log(e, StringUtils.concatWithSpaces(text), LogLevel.Dev, 1);
	}
	
	public void info(Throwable e, Object... text)
	{
		log(e, StringUtils.concatWithSpaces(text), LogLevel.Info, 1);
	}
	
	public void warning(Throwable e, Object... text)
	{
		log(e, StringUtils.concatWithSpaces(text), LogLevel.Warning, 1);
	}
	
	public void error(Throwable e, Object... text)
	{
		log(e, StringUtils.concatWithSpaces(text), LogLevel.Error, 1);
	}
	
	
	
	public void log(Throwable e, String str, LogLevel level)
	{
		log(e, str, level, 1);
	}
	
	public void log(Throwable e, String str, LogLevel level, int offset)
	{
		var logEvent = LogEvent.builder()
				.content(str)
				.level(level)
				.attachedThrowableInfo(AttachedThrowableInfo.of(e))
				.logSource(LogSource.builder()
						.from(new Throwable())
						.offset(offset + 1)
						.logThread(Thread.currentThread())
						.logger(this)
						.creationTime(System.currentTimeMillis())
					.build()
				)
		.build();
		
		log(logEvent);
	}
	
	public void log(LogEvent logEvent)
	{
		eventPreLog.next(logEvent);
		
		if (!logEvent.isCanceled())
		{
			printerFun.apply(logEvent.getContentFully());
			
			eventPostLog.next(logEvent);
		}
	}
}

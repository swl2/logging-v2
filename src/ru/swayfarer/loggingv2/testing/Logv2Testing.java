package ru.swayfarer.loggingv2.testing;

import ru.swayfarer.loggingv2.LoggerV2;
import ru.swayfarer.loggingv2.handlers.logformatter.StandartLogFormatRules;
import ru.swayfarer.loggingv2.manager.LoggingManagerV2;
import ru.swayfarer.swl2.app.ApplicationSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.logger.ILogger;
import ru.swayfarer.swl2.logger.LoggingManager;
import ru.swayfarer.swl2.resource.rlink.RLUtils;
import ru.swayfarer.swl2.threads.ThreadsUtils;

public class Logv2Testing extends ApplicationSWL implements StandartLogFormatRules {

	public static ILogger logV1 = LoggingManager.getLogger();
	public static LoggerV2 logV2 = new LoggerV2();
	
	@Override
	public void startSafe(IExtendedList<String> args) throws Throwable
	{
//		LoggingManagerV2.initializeConfig(RLUtils.createLink("f:sample.yml"));
		
		var logger = LoggingManagerV2.getLogger("SomeLogger");
		
//		Throwable e = new Throwable("Hello World", new NullPointerException());
		
		var i2 = 0;
		var time = 5;
		
		for (int i1 = 0; i1 < 15; i1 ++)
		{
			logger.info("Some info", i2);
			i2 += time;
			ThreadsUtils.sleepSafe(time * 1000);
		}
	}
	
	public static void main(String[] args)
	{
		startApplication(args);
	}
}

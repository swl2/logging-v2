package ru.swayfarer.loggingv2.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import ru.swayfarer.loggingv2.LoggerV2;

@Data
@NoArgsConstructor @AllArgsConstructor
@Builder
@Accessors(chain = true)
public class LogSource {
	
	public LoggerV2 logger;
	public Thread logThread;
	public Throwable from;
	public int offset;
	public long creationTime;

	public StackTraceElement getCallerStacktrace()
	{
		var stacktrace = from.getStackTrace();
		return stacktrace.length > offset && offset >= 0 ? stacktrace[offset] : null;
	}
}

package ru.swayfarer.loggingv2.event;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class AttachedThrowableInfo {

	public StackTraceElement[] stackTraceElements;
	public Throwable attachedThrowable;
	
	public AttachedThrowableInfo cause;
	
	public static AttachedThrowableInfo of(Throwable e)
	{
		if (e == null)
			return null;
		
		return builder()
				.attachedThrowable(e)
				.stackTraceElements(e.getStackTrace())
				.cause(of(e.getCause()))
		.build();
	}
	
	public boolean hasCause()
	{
		return cause != null;
	}
}

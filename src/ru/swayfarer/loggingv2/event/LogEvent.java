package ru.swayfarer.loggingv2.event;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Singular;
import lombok.experimental.Accessors;
import ru.swayfarer.loggingv2.level.LogLevel;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.ExtendedListWrapper;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.observable.events.AbstractCancelableEvent;
import ru.swayfarer.swl2.string.DynamicString;
import ru.swayfarer.swl2.string.StringUtils;

@Data
@NoArgsConstructor @AllArgsConstructor
@Builder
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class LogEvent extends AbstractCancelableEvent {
	
	public LogLevel level;
	
	public String content;
	
	@Singular
	public List<StackTraceEntry> throwableContents;
	
	public LogSource logSource;
	
	public StackTraceElement[] stacktrace;
	
	public AttachedThrowableInfo attachedThrowableInfo;
	
	public IExtendedList<StackTraceEntry> getThrowableContents()
	{
		return new ExtendedListWrapper<>(throwableContents);
	}
	
	public boolean hasAttachedThrowable()
	{
		return attachedThrowableInfo != null;
	}
	
	public String getContentFully()
	{
		var logContent = this.getContent();
		var throwableContents = this.getThrowableContents();
		
		if (this.hasAttachedThrowable() && !CollectionsSWL.isNullOrEmpty(throwableContents))
		{
			var dynString = new DynamicString(logContent);
			
			for (var line : throwableContents)
			{
				dynString.append(StringUtils.lineSplitter);
				dynString.append(line.getContent());
			}
			
			logContent = dynString.toString();
		}
		
		return logContent + "\n";
	}
	
	@Data
	@Builder
	public static class StackTraceEntry {
		public StackTraceElement stackTraceElement;
		public String content;
		public boolean isHeader;
	}
}

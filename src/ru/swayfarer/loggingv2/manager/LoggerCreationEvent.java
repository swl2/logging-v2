package ru.swayfarer.loggingv2.manager;

import lombok.Builder;
import lombok.Data;
import ru.swayfarer.loggingv2.LoggerV2;

@Data
@Builder
public class LoggerCreationEvent {

	public StackTraceElement creationLocation;
	public LoggerV2 logger;
	
}

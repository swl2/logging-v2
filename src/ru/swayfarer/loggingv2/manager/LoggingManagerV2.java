package ru.swayfarer.loggingv2.manager;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

import lombok.SneakyThrows;
import ru.swayfarer.loggingv2.LoggerV2;
import ru.swayfarer.loggingv2.config.LoggingV2Configurator;
import ru.swayfarer.loggingv2.config.auto.LoggingV2AutoConfigurator;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.exceptions.ExceptionsUtils;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1NoR;
import ru.swayfarer.swl2.observable.IObservable;
import ru.swayfarer.swl2.observable.Observables;
import ru.swayfarer.swl2.resource.rlink.ResourceLink;
import ru.swayfarer.swl2.swconf2.helper.SwconfIO;

public class LoggingManagerV2 {

	public static AtomicBoolean isConfigInitialized = new AtomicBoolean();
	public static IExtendedList<LoggerCreationEvent> archivedEvents = CollectionsSWL.createExtendedList();
	public static IObservable<LoggerCreationEvent> eventCreation = Observables.createObservable();
	
	public static LoggerV2 getLogger(String name)
	{
		var logger = new LoggerV2();
		var callerStacktrace = ExceptionsUtils.getCallerStacktrace();
		
		var event = LoggerCreationEvent.builder()
				.creationLocation(callerStacktrace)
				.logger(logger)
		.build();
		
		archivedEvents.add(event);
		eventCreation.next(event);
		
		return logger;
	}
	
	public static boolean isConfigInitialized()
	{
		return isConfigInitialized.get();
	}
	
	@SneakyThrows
	public static void initializeConfig(ResourceLink configLocation)
	{
		if (!isConfigInitialized())
		{
			isConfigInitialized.set(true);
			
			var swconfIO = new SwconfIO();
			
			var configurator = swconfIO.deserialize(LoggingV2Configurator.class, configLocation);
			
			ExceptionsUtils.IfNull(configurator, IOException.class, "Valid logging configurator was not found!");
			
			IFunction1NoR<LoggerCreationEvent> configFun = (event) -> {
				configurator.applyToLogger(event.getCreationLocation(), event.getLogger());
			};
			
			archivedEvents.each(configFun);
			archivedEvents.clear();
			
			eventCreation.subscribe(configFun);
		}
	}
	
	static
	{
		var autoConfigurator = new LoggingV2AutoConfigurator();
		autoConfigurator.configure();
	}
}

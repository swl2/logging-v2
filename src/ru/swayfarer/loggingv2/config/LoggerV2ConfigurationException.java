package ru.swayfarer.loggingv2.config;

@SuppressWarnings("serial")
public class LoggerV2ConfigurationException extends RuntimeException {

	public LoggerV2ConfigurationException() { }

	public LoggerV2ConfigurationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) 
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public LoggerV2ConfigurationException(String message, Throwable cause) 
	{
		super(message, cause);
	}

	public LoggerV2ConfigurationException(String message) 
	{
		super(message);
	}

	public LoggerV2ConfigurationException(Throwable cause) 
	{
		super(cause);
	}

}

package ru.swayfarer.loggingv2.config;

import ru.swayfarer.loggingv2.LoggerV2;
import ru.swayfarer.loggingv2.config.filtering.LogFiltering;
import ru.swayfarer.loggingv2.config.io.LogFileArchiving;
import ru.swayfarer.loggingv2.config.io.LogFileProperty;
import ru.swayfarer.loggingv2.config.printing.LogPrintingConfig;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.resource.rlink.RLUtils;
import ru.swayfarer.swl2.swconf2.helper.SwconfIO;

public class LoggingV2Configurator {

	public IExtendedList<LogConfiguratorEntry> confs = CollectionsSWL.createExtendedList();
	
	public void applyToLogger(StackTraceElement created, LoggerV2 logger)
	{
		var conf = new LogConfiguratorEntry();
		
		for (var entry : confs)
		{
			if (entry.isAcceptingLogger(created))
			{
				conf.merge(entry);
			}
		}
		
		conf.applyToLogger(logger);
	}
	
	public static void main(String[] args) {
		
		var configurator = new LoggingV2Configurator();
		
		var configEntry = new LogConfiguratorEntry();
		var logPrintingConfig = new LogPrintingConfig();
		
		logPrintingConfig.coloringMode = "8bit";
		logPrintingConfig.logFormat = "%levelColor%[%thread%/%level%] -> %content% &c{clear}";
		logPrintingConfig.stacktraceFormat = "        %levelColor%at %class%.%method%&c{PastelCyan} (%file%:%line%) &c{PastelBlue}[~%loc%]&c{h:2}";
		logPrintingConfig.throwableHeaderFormat = "%levelColor%>> &c{PastelCyan}%type%&c{h:1}: %message% &c{clear}";
		
		configEntry.printing = logPrintingConfig;
		
		var logFiltering = new LogFiltering();
		
		logFiltering.maxLevel = "Error";
		logFiltering.minLevel = "Info";
		
		logFiltering.blackList.add("ru.swayfarer.blacklist");
		logFiltering.whiteList.add("mask:*");
		
		logFiltering.stacktraceBlacklist.addAll(
				"$swl.internal",
				"$swl.app",
				"$java.reflect"
		);
		
		configEntry.filtering = logFiltering;
		
		configEntry.sources = CollectionsSWL.createExtendedList();
		configEntry.sources.add("ru.swayfarer");
		
		var logFileProperty = new LogFileProperty();
		
		logFileProperty.delay = "1 sec";
		logFileProperty.encoding = "UTF-8";
		logFileProperty.path = "logs.txt";
		
		var logFileArchiving = new LogFileArchiving().registerDefaultStreamCreationFuns();
		
		logFileArchiving.onDelay = "12 hour";
		logFileArchiving.name = "%date{dd.MM.yyyy-hh.mm.ssss}%-%logName%.gz";
		logFileArchiving.format = "gz";
		logFileArchiving.onFileSize = "15kb";
		logFileArchiving.dir = "logs/";
		
		logFileProperty.archiving = logFileArchiving;
		
		configEntry.files = CollectionsSWL.createExtendedList();
		configEntry.files.add(logFileProperty);
		
		configurator.confs.add(configEntry);
		
		var swconfIO = new SwconfIO();
		swconfIO.serialize(configurator, RLUtils.createLink("f:sample.yml"));
	}
	
}

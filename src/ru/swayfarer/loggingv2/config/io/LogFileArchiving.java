package ru.swayfarer.loggingv2.config.io;

import java.io.OutputStream;

import lombok.Data;
import ru.swayfarer.loggingv2.handlers.io.ArchiverWriteListener;
import ru.swayfarer.loggingv2.handlers.io.FileWriterFun;
import ru.swayfarer.loggingv2.handlers.io.StandartArchiveCondition;
import ru.swayfarer.swl2.binary.BinaryUtils;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedMap;
import ru.swayfarer.swl2.date.DateUtils;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1;
import ru.swayfarer.swl2.resource.file.FileSWL;
import ru.swayfarer.swl2.resource.streams.DataOutputStreamSWL;
import ru.swayfarer.swl2.swconf2.mapper.annotations.CommentedSwconf;
import ru.swayfarer.swl2.swconf2.mapper.annotations.IgnoreSwconf;

@Data
@SuppressWarnings("unchecked")
public class LogFileArchiving {

	@IgnoreSwconf
	public IExtendedMap<String, IFunction1<FileSWL, OutputStream>> registeredStreamCreationFun = CollectionsSWL.createExtendedMap();
	
	@CommentedSwconf("Name of archvie file")
	public String name;
	
	@CommentedSwconf("Format of archive")
	public String format;
	
	@CommentedSwconf("Dir where log files will be located")
	public String dir;
	
	@CommentedSwconf("Delay of file archiving")
	public String onDelay;
	
	@CommentedSwconf("The maximum file size upon which it will be archived")
	public String onFileSize;
	
	public void appendToFun(FileWriterFun fun)
	{
		var streamCreationFun = registeredStreamCreationFun.get(format);
		
		if (streamCreationFun == null)
		{
			streamCreationFun = (f) -> f.append().zip();
		}
		
		var archiver = ArchiverWriteListener.builder()
				.file(fun.file)
				.archiveCreationFun((f) -> new FileSWL(dir + "/" + name.replace("%logName%", fun.file.getName()))) 
				.streamCreationFun(streamCreationFun) 
		.build();
		
		if (onDelay != null)
		{
			archiver.conditions.add(StandartArchiveCondition.every(DateUtils.getMilisis(onDelay)));
		}
		
		if (onFileSize != null)
		{
			archiver.conditions.add(StandartArchiveCondition.maxSize(BinaryUtils.getBytes(onFileSize)));
		}
		
		fun.eventWrite.subscribe(archiver);
	}
	
	public <T extends LogFileArchiving> T registerArchivingFun(String format, IFunction1<DataOutputStreamSWL, OutputStream> fun)
	{
		return registerCreationFun(format, (f) -> fun.apply(f.append()));
	}
	
	public <T extends LogFileArchiving> T registerCreationFun(String format, IFunction1<FileSWL, OutputStream> fun)
	{
		registeredStreamCreationFun.put(format, fun);
		return (T) this;
	}
	
	public <T extends LogFileArchiving> T registerDefaultStreamCreationFuns()
	{
		registerArchivingFun("gz", DataOutputStreamSWL::gz);
		registerArchivingFun("zip", DataOutputStreamSWL::zip);
		
		return (T) this;
	}
	
	{
		registerDefaultStreamCreationFuns();
	}
}

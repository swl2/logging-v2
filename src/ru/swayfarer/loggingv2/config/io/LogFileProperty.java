package ru.swayfarer.loggingv2.config.io;

import java.util.concurrent.atomic.AtomicLong;

import lombok.Data;
import lombok.SneakyThrows;
import ru.swayfarer.loggingv2.LoggerV2;
import ru.swayfarer.loggingv2.config.filtering.LogFiltering;
import ru.swayfarer.loggingv2.handlers.io.FileWriterFun;
import ru.swayfarer.loggingv2.handlers.io.LogWriter;
import ru.swayfarer.loggingv2.handlers.io.WriterThread;
import ru.swayfarer.swl2.date.DateUtils;
import ru.swayfarer.swl2.exceptions.ExceptionsUtils;
import ru.swayfarer.swl2.resource.file.FileSWL;
import ru.swayfarer.swl2.string.StringUtils;
import ru.swayfarer.swl2.swconf2.mapper.annotations.CommentedSwconf;

@Data
public class LogFileProperty {

	@CommentedSwconf("Path to log file")
	public String path;
	
	@CommentedSwconf("Archiving of log file")
	public LogFileArchiving archiving;
	
	@CommentedSwconf("Filtering logs to be written to a file")
	public LogFiltering filtering;
	
	@CommentedSwconf("Saving delay")
	public String delay;
	
	@CommentedSwconf("Encoding of log file")
	public String encoding;
	
	@SneakyThrows
	public void applyToLogger(LoggerV2 logger)
	{
		ExceptionsUtils.IfNot(StringUtils.isEncodingSupported(encoding), IllegalArgumentException.class, "Encoding '" + encoding + "' does not supported!");
		
		var logWriter = new LogWriter();
		var writerFun = FileWriterFun.of(new FileSWL(path), encoding);

		if (archiving != null)
			archiving.appendToFun(writerFun);
		
		var writerThread = WriterThread.builder()
				.sleepTime(new AtomicLong(getWritingDelay()))
				.writerFun(writerFun)
		.build();
		
		logWriter.accesses.add(writerThread.getAccess());
		
		writerThread.start();
		
		logger.eventPostLog.subscribe(logWriter);
	}
	
	public long getWritingDelay()
	{
		return DateUtils.getMilisis(delay);
	}
}

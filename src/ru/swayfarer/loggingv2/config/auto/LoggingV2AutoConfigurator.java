package ru.swayfarer.loggingv2.config.auto;

import ru.swayfarer.loggingv2.manager.LoggingManagerV2;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.resource.rlink.RLUtils;
import ru.swayfarer.swl2.string.StringUtils;
import ru.swayfarer.swl2.string.property.SystemProperty;

public class LoggingV2AutoConfigurator {

	public IExtendedList<String> extensions = CollectionsSWL.createExtendedList(
			"yaml",
			"yml",
			"lua"
	);
	
	public SystemProperty isAutoConfigureEnabledProperty = new SystemProperty("swl2.loggingv2.config.enabled", true);
	
	public SystemProperty configLocationProperty = new SystemProperty("swl2.loggingv2.config.location", "assets/config/swl2/logging-v2.*");
	
	public boolean isAutoConfigureEnabled()
	{
		return isAutoConfigureEnabledProperty.getBooleanValue();
	}
	
	public void configure()
	{
		if (isAutoConfigureEnabled())
		{
			var isFound = false;
			var resourceName = "";
			var prefix = StringUtils.removeLastWhitespaces(configLocationProperty.getValue());
			
			if (!prefix.endsWith("*"))
			{
				isFound = true;
				resourceName = prefix;
			}
			
			for (var ext : extensions)
			{
				resourceName = prefix.replace("*", ext);
				
				if (RLUtils.exists(resourceName))
				{
					isFound = true;
					break;
				}
			}
			
			if (isFound)
			{
				LoggingManagerV2.initializeConfig(RLUtils.createLink(resourceName));
			}
		}
	}
}

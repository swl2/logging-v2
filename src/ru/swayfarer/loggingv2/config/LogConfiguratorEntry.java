package ru.swayfarer.loggingv2.config;

import lombok.Data;
import ru.swayfarer.loggingv2.LoggerV2;
import ru.swayfarer.loggingv2.config.filtering.LogFiltering;
import ru.swayfarer.loggingv2.config.io.LogFileProperty;
import ru.swayfarer.loggingv2.config.printing.LogPrintingConfig;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.string.StringUtils;
import ru.swayfarer.swl2.swconf2.mapper.annotations.CommentedSwconf;

@Data
public class LogConfiguratorEntry {
 
	@CommentedSwconf("Sources to which settings will be applied")
	public IExtendedList<String> sources;
	
	@CommentedSwconf("Logs filtering")
	public LogFiltering filtering;
	
	@CommentedSwconf("Printing settings")
	public LogPrintingConfig printing;
	
	@CommentedSwconf("Settings for writing logs to a file")
	public IExtendedList<LogFileProperty> files;
	
	public void merge(LogConfiguratorEntry entry)
	{
		this.sources = LogEntriesMergeHelper.mergeLists(sources, entry.getSources());
		this.filtering = LogEntriesMergeHelper.merge(filtering, entry.getFiltering());
		this.files = LogEntriesMergeHelper.merge(files, entry.getFiles());
		this.printing = LogEntriesMergeHelper.merge(printing, entry.getPrinting());
	}
	
	public void applyToLogger(LoggerV2 logger)
	{
		if (filtering != null)
		{
			filtering.applyToLogger(logger);
		}
		
		if (printing != null)
		{
			printing.applyToLogger(logger);
		}
		
		if (!CollectionsSWL.isNullOrEmpty(files))
		{
			for (var logFileProperty : files)
			{
				logFileProperty.applyToLogger(logger);
			}
		}
	}
	
	public boolean isAcceptingLogger(StackTraceElement stackTraceElement)
	{
		var className = stackTraceElement.getClassName();
		
		for (var expression : sources)
		{
			if (StringUtils.isMatchesByExpression(expression, className))
				return true;
		}
		
		return false;
	}
}

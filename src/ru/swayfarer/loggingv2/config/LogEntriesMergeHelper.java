package ru.swayfarer.loggingv2.config;

import ru.swayfarer.swl2.collections.extended.IExtendedList;

public interface LogEntriesMergeHelper {

	public static String USE_PREVOUS_STR = "$prevous";
	
	public static <T> T merge(T oldObj, T newObj)
	{
		if (newObj == null)
			return oldObj;
		else
			return newObj;
	}
	
	public static <T extends IMergeableEntry<T>> T merge(T oldObj, T newObj)
	{
		if (oldObj == null)
			return newObj;
		
		if (newObj == null)
			return oldObj;
		
		oldObj.merge(newObj);
		
		return oldObj;
	}
	
	public static IExtendedList<String> mergeLists(IExtendedList<String> oldList, IExtendedList<String> newList)
	{
		if (oldList == null)
			return newList;
		
		if (newList == null)
			return oldList;
		
		if (newList.isEmpty())
			return newList;
		
		if (newList.contains(USE_PREVOUS_STR))
		{
			oldList.addAll(newList);
			return oldList;
		}
		
		return newList;
	}
	
	public static interface IMergeableEntry<T> {
		public void merge(T newObj);
	}
	
}

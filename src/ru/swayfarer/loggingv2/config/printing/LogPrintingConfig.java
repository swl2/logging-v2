package ru.swayfarer.loggingv2.config.printing;

import lombok.Data;
import ru.swayfarer.loggingv2.LoggerV2;
import ru.swayfarer.loggingv2.config.LogEntriesMergeHelper;
import ru.swayfarer.loggingv2.config.LogEntriesMergeHelper.IMergeableEntry;
import ru.swayfarer.loggingv2.handlers.colorizer.LogColorizer;
import ru.swayfarer.loggingv2.handlers.colorizer.StandartCodeToColorFuns;
import ru.swayfarer.loggingv2.handlers.logformatter.ColorizedLogFormatFun;
import ru.swayfarer.loggingv2.handlers.logformatter.LogFormatter;
import ru.swayfarer.swl2.exceptions.ExceptionsUtils;
import ru.swayfarer.swl2.swconf2.mapper.annotations.CommentedSwconf;

@Data
public class LogPrintingConfig implements IMergeableEntry<LogPrintingConfig> {

	@CommentedSwconf(
			"-------------------------------------------------------- \n"
		  + "Format of log message. The \n"
		  + "%content% will be replaced by log content \n"
		  + "%thread% by log thread \n"
		  + "%level% by log level \n"
		  + "%name% by logger name \n"
		  + "%levelColor% by default color of level message \n"
		  + "&c{Color} by color prefix \n"
		  + "&c{clear} by color clearing prefix \n"
		  + "$c{h:num} by color prefix from use history at pos num \n"
		  + "---------------------------------------------------------"
	)
	public String logFormat;
	
	@CommentedSwconf(
			  "Format of stacktrace element. The \n"
			+ "%class% will be replaced by class name \n"
			+ "%line% by line \n"
			+ "%file% by source file \n"
			+ "%method% by method name \n"
			+ "%loc% by source location (e.g. jar file or folder) \n"
	)
	public String stacktraceFormat;
	
	@CommentedSwconf(
			"Format of stacktrace header. The \n"
		  + "%message% will be replaced by throwable message \n"
		  + "%type% by type of exception"
	)
	public String throwableHeaderFormat;
	
	@CommentedSwconf("Mode by which colors from &c{} expressions will be used. Now may be: anci, 8bit")
	public String coloringMode;
	
	@Override
	public void merge(LogPrintingConfig newObj) {
		
		logFormat = LogEntriesMergeHelper.merge(logFormat, newObj.getLogFormat());
		stacktraceFormat = LogEntriesMergeHelper.merge(stacktraceFormat, newObj.getLogFormat());
		coloringMode = LogEntriesMergeHelper.merge(coloringMode, newObj.getLogFormat());
	}
	
	public void applyToLogger(LoggerV2 logger)
	{
		var logFormatter = new LogFormatter().registerDefaultFormatRules();
		var codeToColorFun = StandartCodeToColorFuns.of(coloringMode);
		
		ExceptionsUtils.IfNull(codeToColorFun, IllegalArgumentException.class, "Coloring mode '" + coloringMode + "' is not registered! Please, register it or use another mode! Available modes:", StandartCodeToColorFuns.registeredColorFuns.keySet());
		
		if (throwableHeaderFormat != null)
		{
			var coloredLogFormatFun = new ColorizedLogFormatFun().registerDefaultPreProcessRules();
			coloredLogFormatFun.logColorizer.codeToColorFun = codeToColorFun;
			coloredLogFormatFun.setFormat(throwableHeaderFormat);
			logFormatter.stackTraceHeaderFormatGetFun = coloredLogFormatFun;
		}
		
		if (logFormat != null)
		{
			var coloredLogFormatFun = new ColorizedLogFormatFun().registerDefaultPreProcessRules();
			coloredLogFormatFun.logColorizer.codeToColorFun = codeToColorFun;
			coloredLogFormatFun.setFormat(logFormat);
			logFormatter.formatGetFun = coloredLogFormatFun;
		}
		
		if (stacktraceFormat != null)
		{
			var coloredStackTraceFormatFun = new ColorizedLogFormatFun().registerDefaultPreProcessRules();
			coloredStackTraceFormatFun.logColorizer.codeToColorFun = codeToColorFun;
			coloredStackTraceFormatFun.setFormat(stacktraceFormat);
			logFormatter.stackTraceFormatGetFun = coloredStackTraceFormatFun;
		}
		
		logger.eventPreLog.subscribe(logFormatter);
		logger.eventPreLog.subscribe(new LogColorizer().setCodeToColorFun(codeToColorFun));
	}
}

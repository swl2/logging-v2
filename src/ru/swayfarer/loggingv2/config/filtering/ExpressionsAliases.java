package ru.swayfarer.loggingv2.config.filtering;

import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1NoR;

public class ExpressionsAliases {

	public static IExtendedList<IFunction1NoR<IExtendedList<String>>> registeredExpressionPostprocessors = CollectionsSWL.createExtendedList();
	
	public static ExpressionsAliases registerPostProcessor(IFunction1NoR<IExtendedList<String>> fun)
	{
		registeredExpressionPostprocessors.addExclusive(fun);
		return null;
	}
	
	public static ExpressionsAliases postProcess(IExtendedList<String> expressions)
	{
		for (var fun : registeredExpressionPostprocessors)
		{
			fun.apply(expressions);
		}
		
		return null;
	}
	
	public static void registerDefaultPostProcessors()
	{
		registerPostProcessor((list) -> {
			if (list.contains("$swl.internal"))
			{
				list.addAll(
						"ru.swayfarer.swl2.logger",
						"ru.swayfarer.swl2.functions.GeneratedFunctions",
						"ru.swayfarer.swl2.asm.classloader.ClassLoaderSWL",
						"ru.swayfarer.swl2.classes.ReflectionUtils"
				);
			}
		});
		
		registerPostProcessor((list) -> {
			if (list.contains("$swl.app"))
			{
				list.addAll(
						"ru.swayfarer.swl2.app.ApplicationSWL"
				);
			}
		});
		
		registerPostProcessor((list) -> {
			if (list.contains("$java.reflect"))
			{
				list.addAll(
						"jdk.internal.reflect",
						"java.lang.reflect"
				);
			}
		});
	}
	
	static
	{
		registerDefaultPostProcessors();
	}
}

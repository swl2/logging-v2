package ru.swayfarer.loggingv2.config.filtering;

import lombok.Data;
import ru.swayfarer.loggingv2.LoggerV2;
import ru.swayfarer.loggingv2.config.LogEntriesMergeHelper;
import ru.swayfarer.loggingv2.config.LogEntriesMergeHelper.IMergeableEntry;
import ru.swayfarer.loggingv2.event.LogEvent;
import ru.swayfarer.loggingv2.handlers.filtering.LogFilter;
import ru.swayfarer.loggingv2.handlers.stacktracefilter.StackTraceFilter;
import ru.swayfarer.loggingv2.level.LogLevel;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.string.StringUtils;
import ru.swayfarer.swl2.swconf2.mapper.annotations.CommentedSwconf;
import ru.swayfarer.swl2.swconf2.mapper.annotations.IgnoreSwconf;

@Data
public class LogFiltering implements IMergeableEntry<LogFiltering> {
	
	@IgnoreSwconf
	public volatile Integer minLevelInt;
	
	@IgnoreSwconf
	public volatile Integer maxLevelInt;
	
	@CommentedSwconf("Name of minimum level that will be shown")
	public String minLevel;
	
	@CommentedSwconf("Name of maximum level that will be shown")
	public String maxLevel;
	
	@CommentedSwconf("Whitelist of classes, logs from which will be shown")
	public IExtendedList<String> whiteList = CollectionsSWL.createExtendedList();
	
	@CommentedSwconf("Blacklist of classes, logs from which will be hidden")
	public IExtendedList<String> blackList = CollectionsSWL.createExtendedList();
	
	@CommentedSwconf("Blacklist of classes, stacktrace from which will be hidden")
	public IExtendedList<String> stacktraceBlacklist = CollectionsSWL.createExtendedList();
	
	public void applyToLogger(LoggerV2 logger)
	{
		var logFilter = new LogFilter();
		
		var filters = logFilter.filters;
		
		ExpressionsAliases.postProcess(blackList);
		ExpressionsAliases.postProcess(whiteList);
		ExpressionsAliases.postProcess(stacktraceBlacklist);
		
		filters.add(this::isAccepting);
		
		logger.eventPreLog.subscribe(logFilter);
		
		if (!CollectionsSWL.isNullOrEmpty(stacktraceBlacklist))
		{
			var stackTraceFilter = new StackTraceFilter();
			stackTraceFilter.stackTraceBlocks.addAll(stacktraceBlacklist);
			logger.eventPreLog.subscribe(stackTraceFilter);
		}
	}
	
	public void merge(LogFiltering filtering)
	{
		this.minLevel = filtering.minLevel;
		this.maxLevel = filtering.maxLevel;
		
		this.whiteList = LogEntriesMergeHelper.mergeLists(whiteList, filtering.getWhiteList());
		this.blackList = LogEntriesMergeHelper.mergeLists(blackList, filtering.getBlackList());
	}
	
	public int getMaxLevelInt()
	{
		if (maxLevelInt == null)
		{
			synchronized (this)
			{
				if (maxLevelInt == null)
				{
					var level = LogLevel.of(maxLevel);
					
					if (level == null)
					{
						maxLevelInt = Integer.MAX_VALUE;
					}
					else
					{
						maxLevelInt = level.getWeight();
					}
				}
			}
		}
		
		return maxLevelInt;
	}
	
	public int getMinLevelInt()
	{
		if (minLevelInt == null)
		{
			synchronized (this)
			{
				if (minLevelInt == null)
				{
					var level = LogLevel.of(minLevel);
					
					if (level == null)
					{
						minLevelInt = Integer.MIN_VALUE;
					}
					else
					{
						minLevelInt = level.getWeight();
					}
				}
			}
		}
		
		return minLevelInt;
	}
	
	public boolean isAccepting(LogEvent logEvent)
	{
		var logLevel = logEvent.getLevel();
		var logWeight = logLevel.getWeight();
		
		var minLevel = getMinLevelInt();
		var maxLevel = getMaxLevelInt();
		
		if (logWeight < minLevel)
			return false;
		
		if (logWeight > maxLevel)
			return false;
		

		if (!CollectionsSWL.isNullOrEmpty(blackList))
		{
			var className = logEvent.getLogSource().getCallerStacktrace().getClassName();
			
			for (var expression : blackList)
			{
				if (StringUtils.isMatchesByExpression(expression, className))
				{
					return false;
				}
			}
		}
		
		if (!CollectionsSWL.isNullOrEmpty(whiteList))
		{
			var className = logEvent.getLogSource().getCallerStacktrace().getClassName();
			var isMatchingSome = false;
			
			for (var expression : whiteList)
			{
				if (StringUtils.isMatchesByExpression(expression, className))
				{
					isMatchingSome = true;
				}
			}
			
			if (!isMatchingSome)
			{
				return false;
			}
		}
		
		return true;
	}
	
}
